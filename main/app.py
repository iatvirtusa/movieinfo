from flask import Flask, request, jsonify,flash,redirect,send_file,render_template
#import json
#i#mport mysql.connector
#import os
#from typing import List, Dict
#Test Comment1 
import psycopg2
app = Flask(__name__, template_folder='templates')

new_url1 = 'http://10.61.10.6:5001/DescriptionandRating/'
new_url2 = 'http://10.61.10.6:5002/ShowTimings/'
new_url3 = 'http://10.61.10.6:5003/Cast/'
new_url4 = 'http://10.61.10.6:5004/OTTPlatform/'

def getPostgresConnection():
    return psycopg2.connect(
        host="10.61.10.6",
        database="movie",
        user="postgres",
        password="postgres")

@app.route("/", methods=["GET", "POST"])
def login():
    #line_info()
    if request.method == "GET":
        return render_template("login.html")
    else:
        # Set permanent session
        # session.permanent = True
        name = request.form.get("username")
        password = request.form.get("password")
        if name and password:
            conn = getPostgresConnection()
            c = conn.cursor()

            c.execute("SELECT * FROM login WHERE username = %s", [name])

            x = c.fetchall()
            print("db_result : ", x)
            if len(x) != 1:
                return render_template("login.html")

            return redirect("/Home")

@app.route("/Home", methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        movieid = request.form['movie']
        print(movieid)
        option = request.form['info_options']
        print(option)
        if(option=='DescriptionandRating'):
            return redirect(new_url1 + movieid, code=302)
        elif(option=='ShowTimings'):
            return redirect(new_url2 + movieid, code=302)
        elif (option == 'Cast'):
            return redirect(new_url3 + movieid, code=302)
        elif (option == 'OttPlatform'):
            return redirect(new_url4 + movieid, code=302)
    return render_template('home.html')

@app.route("/AddMovie", methods=["GET", "POST"])
def addmovie():
    #line_info()
    if request.method == 'GET':
        return render_template("addmovie.html")
    else:
        movie_id = request.form.get('movie_id')
        movie_name = request.form.get("movie_name")
        movie_director = request.form.get("movie_director")
        movie_rating = request.form.get('movie_rating')
        movie_show_time = request.form.get("movie_show_time")
        movie_theater = request.form.get("movie_theater")
        movie_cast_details = request.form.get("movie_cast_details")
        movie_ott_details = request.form.get("movie_ott_details")
        print("movie_id : ",movie_id, "movie_name : ", movie_name, "movie_director : ", movie_director,
              "movie_rating : ",movie_rating, "movie_show_time : ",movie_show_time,
              "movie_theater : ", movie_theater, "movie_cast_details : ", movie_cast_details,
              "movie_ott_details : ",movie_ott_details)
        db = getPostgresConnection()
        cur = db.cursor()
        cur.execute("INSERT into movie_des (id, rating, title, director) VALUES"
                    "(%s, %s, %s, %s)", [movie_id, movie_rating, movie_name, movie_director])
        db.commit()

        cur.execute("INSERT into show_time (id, title, showtime, theater) VALUES"
                    "(%s, %s, %s, %s)", [movie_id, movie_name, movie_show_time, movie_theater])
        db.commit()

        cur.execute("INSERT into cast_info (id, title, cast_detail) VALUES"
                    "(%s, %s, %s)", [movie_id, movie_name, movie_cast_details])
        db.commit()

        cur.execute("INSERT into ott_info (id, title, available_Ott_detail) VALUES"
                    "(%s, %s, %s)", [movie_id, movie_name, movie_ott_details])
        db.commit()

        cur = db.cursor()
        cur.close()
        db.close()


        return redirect("/Home")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
