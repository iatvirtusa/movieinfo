from flask import Flask, request, jsonify,flash,redirect,send_file,render_template
import json
import psycopg2
import os
#from typing import List, Dict
#test comment9
app = Flask(__name__, template_folder='templates')

def getPostgresConnection():
    return psycopg2.connect(
            host="10.61.10.6",
            database="movie",
            user="postgres",
            password="postgres")

@app.route("/DescriptionandRating/<movieid>", methods=['GET', 'POST'])
def firstpage_movie_info(movieid):
    print(movieid)
    db = getPostgresConnection()
    sqlstr = "SELECT * from movie_des WHERE id='" + movieid + "'"
    print(sqlstr)
    cur = db.cursor()
    cur.execute(sqlstr)
    rv = cur.fetchall()
    print(rv)
    for result in rv:
        dic = {'Id':result[0],'Rating':result[1],'Title':result[2],'Director':result[3]}
    temp = list((str(j) for i in rv for j in i))
    res = len(temp)
    if res == 0:
        return render_template("first_page_error.html")
    db.close()
    return render_template("first_page_result.html",result = dic)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)
