CREATE DATABASE movie
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_India.1252'
    LC_CTYPE = 'English_India.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE movie
    IS 'This is the database for the Movie app';

CREATE TABLE public.login
(
    username character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default"
)

CREATE TABLE public.cast_info
(
    id integer,
    title character varying COLLATE pg_catalog."default",
    cast_detail character varying COLLATE pg_catalog."default"
)

CREATE TABLE public.ott_info
(
    id integer,
    title character varying COLLATE pg_catalog."default",
    available_ott_detail character varying COLLATE pg_catalog."default"
)

CREATE TABLE public.show_time
(
    id integer,
    title character varying COLLATE pg_catalog."default",
    showtime double precision,
    theater character varying COLLATE pg_catalog."default"
)

CREATE TABLE public.movie_des
(
    id integer,
    rating double precision,
    title character varying COLLATE pg_catalog."default",
    director character varying COLLATE pg_catalog."default"
)


INSERT INTO movie_des
  (id, rating, title, director)
VALUES
  ('101', '4.0', 'The Good Dinosaur', 'Peter Sohn'),
  ('102', '3.0', 'The Martian', 'Ridley Scott'),
  ('103', '2.0', 'The Night Before', 'Jonathan Levine'),
  ('104', '2.0', 'Creed', 'Ryan Coogler');

INSERT INTO show_time
  (id, title, showtime, theater)
VALUES
  ('101', 'The Good Dinosaur', '13.00', 'ARRS'),
  ('102', 'The Martian', '16.00', 'KS'),
  ('103', 'The Night Before', '22.00', 'RajaSabari'),
  ('104', 'Creed', '10.00', 'ARRS');

INSERT INTO cast_info
  (id, title, cast_detail)
VALUES
  ('101', 'The Good Dinosaur', 'Raymond Ochoa, Sam Elliott, Anna Paquin'),
  ('102', 'The Martian', 'Matt Damon, Jessica Chastain, Kate Mara'),
  ('103', 'The Night Before', 'Seth Rogen, Joseph Gordon-Levitt, Anthony Mackie'),
  ('104', 'Creed', 'Michael B. Jordan, Sylvester Stallone, Tessa Thompson');

INSERT INTO ott_info
  (id, title, available_ott_details)
VALUES
  ('101', 'The Good Dinosaur', 'Netflix'),
  ('102', 'The Martian', 'Amazon Prime'),
  ('103', 'The Night Before', 'Disney plus Hotstar'),
  ('104', 'Creed', 'Netflix');

INSERT INTO login
  (username, password)
VALUES
  ('admin', 'admin@123');
