# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 21:12:44 2020

@author: VIishnulalManoharlal
"""
from flask import Flask, request, jsonify,flash,redirect,send_file,render_template
import json
import psycopg2
import os
#from typing import List, Dict
#Test comment
app = Flask(__name__, template_folder='templates')

def getPostgresConnection():
    return psycopg2.connect(
        host="10.61.10.6",
        database="movie",
        user="postgres",
        password="postgres")


@app.route("/OTTPlatform/<movieid>", methods=['GET', 'POST'])
def fourthpage_movie_info(movieid):
    print(movieid)
    db = getPostgresConnection()
    sqlstr = "SELECT * from ott_info WHERE id='" + movieid + "'"
    print(sqlstr)
    cur = db.cursor()
    cur.execute(sqlstr)
    rv = cur.fetchall()
    for result in rv:
        dic = {'Id':result[0],'Title':result[1],'Available OTT Platform':result[2]}
    temp = list((str(j) for i in rv for j in i))
    res = len(temp)
    if res == 0:
        return render_template("fourth_page_error.html")
    db.close()
    return render_template("fourth_page_result.html",result = dic)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5004)
